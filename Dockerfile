# syntax=docker/dockerfile:1
FROM harbor.its.utexas.edu/base-images/ut-python:3.8

ENV PYTHONDONTWRITEBYTECODE=yes
WORKDIR /app

COPY --chown=python:python test_requirements.txt .

RUN pip3 install --user --no-cache-dir -r test_requirements.txt

CMD coverage run -m pytest; coverage html