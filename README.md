# SEE Core Eligibility Library

## What is this repository for?
This is the core SEE library which is share by [SEE Batch (APP007)](https://bitbucket.org/utasmpint/app007/src/master/)
and the [SEE Online API](https://github.austin.utexas.edu/asmp-int/se_api).

## How do I get set up?
In order to work with this library, after cloning it locally, build the image using `docker build --tag eligibility .`.
Once the image is built, you can run the existing test suite using `docker run -v "$PWD":/app eligibility` since the 
pytest commands are built into the Dockerfile. You can then navigate to the folder `htmlcov` in the app and open 
`index.html` to view the test coverage. Or you can use `docker run -it -v "$PWD":/app eligibility bash` to open a bash 
session in the container and run things like `pytest` and `pytest --cov` or `coverage run -m pytest` and `coverage html`
manually. 

## Testing in apps ##
Once you have changes you would like to validate in the context of one or both of the SEE apps, you can do so by 
running either app locally and pointing them to your local eligibilty library. See the instructions in those repos
for details. 

## Contribution guidelines
Tests are required for any changes you contribute. The current coverage level is at least 98%, and the expectation
is that your contributions will not reduce this coverage level. 

## Migration ##
__*Important*__  
Changes to this library must be pulled into both SEE Batch and SEE Online via their respective requirements.txt files.
Therefore testing of any changes to this library must be tested in the context of both applications.

Once a change has been tested and approved, create a new tag of this library and update the requirements.txt of both
SEE applications to pull in that tag.

## Who do I talk to?
* Repo owner or admin
* Todd is the SEE code expert. Sara is learning. 
