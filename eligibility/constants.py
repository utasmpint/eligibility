FALL_START = '9/1'

SPRING_START = '1/16'

SUMMER_START = '6/1'

SUMMER_2_START = '7/16'

WS_NA_SPRING_START = '1/1'

"""
For the purposes of eligibility, students with a citizenship status of 0
(US citizen) or 3 (permanent resident) are considered domestic (D). Students
with a status of 2 (non-US citizen) are considered foreign (F).
"""
RESIDENCY = {'0': 'D', '2': 'F', '3': 'D'}

JOB_FAMILIES = {
    'undergrad_aca_nt': 'Undergraduate Student Academic',
    'undergrad_non_aca': 'Undergraduate Student Non-Academic',
    'ws': 'Work-Study',
    'professional_aca': 'Professional Student Academic',
    'professional_non_aca': 'Professional Student Non-Academic',
    }

# Used in APP007-SEE-Online OnlineJobData
JOB_FAMILY_MAP = {
    'RG_Graduate_Student_Academic_Non_Teaching' : 'Graduate Student Academic - Non-Teaching',
    'T_Teaching_Assistants' : 'Teaching Assistant',
    'I_Assistant_Instructor' : 'Assistant Instructor',
    'S_Work_Study' : 'Work-Study',
    'SU_Undergraduate_Student_Non_Academic' : ('Undergraduate Student '
        'Non-Academic'),
    'RU_Undergraduate_Student_Academic_Non_Teaching' : ('Undergraduate '
         'Student Academic'),
    'RP_Professional_Student_Academic' : ('Professional Student Academic'), 
    'SP_Professional_Student_Non_Academic' : ('Professional Student '
        'Non-Academic'), 
    }

DEFAULT_HOURS_STATUS = {'pre_deadline': 'non-crit', 'post_deadline': 'crit'}

WS_STATE_EX_CODE_ACA = ('Student Academic - Non-Teaching - (State Expenditure '
    + 'Classification)')

WS_STATE_EX_CODE_NON_ACA = ('Student Non-Academic - (State Expenditure '
    + 'Classification)')

WS_FULL_TIME = 'Full Time Work-Study - (Full Time Work-Study)'

JOB_CLASS_IDS = {
    'non_aca': ('STATE_EXPENDITURE_CLASSIFICATION_STUDENT_NON'
        + '_ACADEMIC'),
    'aca': ('STATE_EXPENDITURE_CLASSIFICATION_STUDENT_ACADEMIC_NON'
        + '_TEACHING'),
    'full_time': 'WORK-STUDY_FULL-TIME'
    }

JOB_CLASSIFICATION = {
    JOB_CLASS_IDS['non_aca']: WS_STATE_EX_CODE_NON_ACA,
    JOB_CLASS_IDS['aca']: WS_STATE_EX_CODE_ACA,
    JOB_CLASS_IDS['full_time']: WS_FULL_TIME,
    }

MAX_WORK_HOURS = {
    'aca': 20, # is 40 in summer
    'non_aca': 40,
    'non_aca_foreign': 20, # is 40 in summer
    'ws_aca': 19,
    'ws_aca_ft': 40,
    'ws_non_aca': 19,
    'ws_non_aca_ft': 40,
    }

TOTAL_WORK_HOURS = {
    'aca': 20,
    'non_aca': 40,
    'non_aca_foreign': 20, #TODO should this be 40?
    'ws_aca': 20, # is 19 in summer
    'ws_aca_ft': 40,
    'ws_non_aca': 40, # is 19 in summer
    'ws_non_aca_ft': 40,
    }

QUANTITY_OF_WORK = { # TODO Not used
    }

LONG_SEM_CREDIT_HOURS = {
    # Undergrad Non-Academic
    'U0073': [6,  DEFAULT_HOURS_STATUS],
    'U0074': [6,  DEFAULT_HOURS_STATUS],
    'U0075': [6,  DEFAULT_HOURS_STATUS],
    'U0076': [6,  DEFAULT_HOURS_STATUS],
    'U0077': [6,  DEFAULT_HOURS_STATUS],
    'U0078': [6,  DEFAULT_HOURS_STATUS],
    'U0079': [6,  DEFAULT_HOURS_STATUS],
    'U0098': [6,  DEFAULT_HOURS_STATUS],
    'U0073S': [6,  DEFAULT_HOURS_STATUS],
    'U0098S': [6,  DEFAULT_HOURS_STATUS],
    # Undergrad Academic
    'U0066': [12,  DEFAULT_HOURS_STATUS],
    'U0070': [12,  DEFAULT_HOURS_STATUS],
    'U0095': [12,  DEFAULT_HOURS_STATUS],
    # Professional Non-Academic
    'P0074': [6,  DEFAULT_HOURS_STATUS],
    'P0075': [6,  DEFAULT_HOURS_STATUS],
    'P0076': [6,  DEFAULT_HOURS_STATUS],
    'P0077': [6,  DEFAULT_HOURS_STATUS],
    # Professional Academic
    'P0066': [12,  DEFAULT_HOURS_STATUS],
    'P0067': [12,  DEFAULT_HOURS_STATUS],
    'P0070': [12,  DEFAULT_HOURS_STATUS],
    'P0095': [12,  DEFAULT_HOURS_STATUS],
    }

SUMMER_CREDIT_HOURS = { #TODO Not used
    } 
